# Wolf Install Scripts

This is a small suite of scripts aimed at automating some menial
tasks when installing [Wolf Linux](https://wolflinux.org).

## License

See COPYING for details.
